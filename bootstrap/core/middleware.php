<?php
// Application middleware

// Adiciona à Aplicação o Middleware de Validação de Erros no Formulário 
$app->add(new \App\Middleware\ValidationErrorsMiddleware($container));

// Adiciona à Aplicação o Middleware de Persistência dos Dados antigos do Formulário 
$app->add(new \App\Middleware\OldInputMiddleware($container));

// Adiciona à Aplicação o Middleware para gerar os campos de formulário para proteção CSRF
// Deve ser adicionado antes do Middleware Oficial do Slim-CSRF, caso contrário ocorre erro
$app->add(new \App\Middleware\CsrfViewMiddleware($container));

// Adiciona um componente PHP component para proteger a aplicação de CSRF (cross-site request forgery)
// Registro para todas as rotas
$app->add($container->get('csrf'));

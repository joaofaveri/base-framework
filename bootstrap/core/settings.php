<?php

// Carrega as Constantes para Conexão com Banco de Dados
require BASEPATH . '/bootstrap/core/dbconfig.php';


return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header
        
        // Configurações do Monolog
        'logger' => [
            'name' => 'app_log',
            'path' => BASEPATH . '/logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],
        
        // Configurações do Twig-View
        'twig-view' => [
            'templates' => BASEPATH . '/templates',
            // 'cache' => BASEPATH . '/tmp/cache/views',
            'cache' => false,
        ],
        
        // Configurações do Eloquent
        'db' => [
            'driver' => DB_DRIVER,
            'host' => DB_HOST,
            'database' => DB_NAME,
            'username' => DB_USERNAME,
            'password' => DB_PASSWORD,
            'charset'   => DB_CHARSET,
            'collation' => DB_COLLATION,
            'prefix'    => DB_PREFIX,
        ],
    ],
];
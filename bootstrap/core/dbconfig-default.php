<?php

 /**
  *
  * Constantes para acesso ao Banco de Dados
  * Esse arquivo deve ser renomeado para dbconfig.php e os dados de conexão devem ser
  * informados pelo usuário do base-framework
  *
  * @author  João Paulo de Fáveri <joao.faveri@gmail.com>
  *
  */

define('DB_DRIVER', ''); // O Eloquent suporta MySQL, PostgreSQL, SQLite e SQL Server
define('DB_HOST', ''); // Endereço para o Banco de Dados
define('DB_NAME', ''); // Nome do Banco de Dados que será utilizado pela Aplicação
define('DB_USERNAME', ''); // Usuário para acesso ao Banco de Dados. Evitar root
define('DB_PASSWORD', ''); // Senha
define('DB_CHARSET', ''); // Conjunto de caracteres. Recomenda-se utilizar utf8
define('DB_COLLATION', ''); // Forma de armazenamento dos caracteres. Recomenda-se utilizar utf8_unicode_ci
define('DB_PREFIX', ''); // Prefixo para o nome das tabelas no Banco de Dados

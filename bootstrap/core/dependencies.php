<?php
// DIC configuration
$container = $app->getContainer();

// Inicia o Eloquent
$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection($container['settings']['db']);
$capsule->setAsGlobal();
$capsule->bootEloquent();

// Monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

// Autenticação de Usuários
$container['auth'] = function ($c) {
    return new \App\Auth\Auth;
};

// Slim-Flash Mensagens
$container['flash'] = function ($c) {
    return new \Slim\Flash\Messages;
};

// Twig-View
$container['view'] = function ($c) {
    $settings = $c->get('settings')['twig-view'];
    $view = new \Slim\Views\Twig($settings['templates'], [
        'cache' => $settings['cache'],
        'debug' => true,
    ]);
    // Instantiate and add Slim specific extension
    $basePath = rtrim(str_ireplace('index.php', '', $c->get('request')->getUri()->getBasePath()), '/');
    $view->addExtension(new \Slim\Views\TwigExtension($c->get('router'), $basePath));
    // Habilitando a função para Debug
    $view->addExtension(new \Twig_Extension_Debug());
    
    // Disponibiliza a Classe Auth para todos os templates
    $view->getEnvironment()->addGlobal('auth', [
        'check' => $c->auth->check(),
        'user' => $c->auth->user()
    ]);
    
    // Disponibiliza Mensagens Flash para todos os templates
    $view->getEnvironment()->addGlobal('flash', $c->flash);
    
    return $view;
};

// Service factory for the ORM Eloquent
$container['db'] = function ($c) {
    return $capsule;
};

// Injeção dos Controllers da Aplicação para facilitar as rotas
// Home
$container['HomeController'] = function ($c) {
    return new \App\Controllers\HomeController($c);
};

// Formulário de Registro de Usuário
$container['AuthController'] = function ($c) {
    return new \App\Controllers\Auth\AuthController($c);
};

// Formulário de Alteração de Senha
$container['PasswordController'] = function ($c) {
    return new \App\Controllers\Auth\PasswordController($c);
};

// Validator
$container['validator'] = function ($c) {
    return new \App\Validation\Validator($c);
};

// Slim-CSRF
$container['csrf'] = function ($c) {
    return new \Slim\Csrf\Guard;
};

<?php
// Requer o arquivo de autoload do Composer
require __DIR__ . '/../vendor/autoload.php';

// Carrega o arquivo de configurações da Aplicação
$settings = require __DIR__ . '/../bootstrap/core/settings.php';

// Carrega uma instância da Aplicação
$app = new \Slim\App($settings);

// Carrega as dependências da Aplicação
require __DIR__ . '/../bootstrap/core/dependencies.php';

// Registra os Middlewares da Aplicação
require __DIR__ . '/../bootstrap/core/middleware.php';

// Registra o namespace para as Regras Personalizadas de Validação
require __DIR__ . '/../bootstrap/core/rules.php';

// Registra as Rotas da Aplicação
require __DIR__ . '/../app/routes.php';

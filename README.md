# base-framework

Base de código para o desenvolvimento de aplicações em PHP, tendo como referência o Slim Framework.
O objetivo ter uma base comum para o desenvolvimento, contendo as tarefas repetitivas, tais como instalação de dependências, configuração de container e templates e o sistema de autenticação de usuário.

## Instalação da Aplicação

Para instalar o framework base para desenvolvimento de aplicações, é necessário o composer e a utilização do seguinte comando:

```
composer create-project joao.faveri/base-framework [pasta-de-destino]
```
Substituir `[pasta-de-destino]` pelo nome do diretório onde a aplicação será instalada. 

## Versão 2.4.0

* Criação do módulo de logout de usuário da Aplicação
* Inclusão do Slim/Flash, para apresentação de mensagens
* Inclusão do módulo de alteração da senha do usuário da Aplicação
* Proteção das rotas que exigem autenticação do usuário

## Versão 2.3.0

* Criação do módulo de registro de usuário da Aplicação
* Criaçao do módulo de login de usuário da Aplicação
* Inclusão do Respect/Validation
* Implementação do processo de validação de formulários

## Versão 2.2.0

* Apenas para teste de versionamento

## Versão 2.1.1

* Apenas para teste de versionamento

## Versão 2.1.0

* Inclusão do Eloquent
* Configuração de um Model de teste para usuários, com tabela de exemplo


## Versão 2.0.0

* Correções na estrutura de pastas e arquivos
* Criação do bootstrap para inicialização da Aplicação
* Inclusão de rota a partir de Classes Controllers
* Remoção do Guzzle como requisito para o base-framework


## Versão 1.0.0

* Primeira base para o desenvolvimento de Aplicação, inspirada no Slim-Skeleton
* Inclusão do Slim Framework
* Inclusão do Monolog
* Inclusão do Twig-View
* Inclusão do Guzzle
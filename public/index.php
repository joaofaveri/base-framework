<?php
ini_set("display_errors","1"); // Apenas em Desenvolvimento
ERROR_REPORTING(E_ALL); // Apenas em Desenvolvimento

// Inicia a Sessão
// Verificar o funcionamento de Sessão no SLIM
session_start();

// Cria constante para o Diretorio Base do Aplicativo
// Sempre direciono a raiz do servidor para a pasta public
// Para evitar problema nas configurações, usar a variável
// com o objetivo de alcançar os diretórios na raiz do sistema
define('BASEPATH', dirname(__DIR__));

// Requer os arquivos necessários para rodar a Aplicação
require __DIR__ . '/../bootstrap/app.php';

// Rodar a Aplicação
$app->run();

<?php

namespace App\Controllers\Auth;

use App\Controllers\Controller;
use App\Models\User;
use Respect\Validation\Validator as v;

class PasswordController extends Controller
{
    public function getChangePassword($request, $response) {
        return $this->view->render($response, 'auth/password/change.html');
        
    }
    
    public function postChangePassword($request, $response) {
        
        // Informa as regras de validação do formulário de mudança de senha
        $validation = $this->validator->validate($request, [
            'password_old' => v::noWhitespace()->notEmpty()->matchesPassword($this->auth->user()->password),
            'password' => v::noWhitespace()->notEmpty(),
        ]);
        
        // Verifica se houve erro de validação
        if ($validation->failed()) {
            // Ocorrendo erro, redireciona para o formulário de mudança de senha
            return $response->withRedirect($this->router->pathFor('password.change'));
        }
        
        // Chama a função que está no model User
        $this->auth->user()->setPassword($request->getParam('password'));
        
        // Cria mensagem avisando a senha do usuário foi alterada com sucesso
        $this->flash->addMessage('success', 'Senha alterada com sucesso!');
        
        // Após salvar os dados no Banco, redireciona para a home da administração
        return $response->withRedirect($this->router->pathFor('admin.home'));
        
    }

}
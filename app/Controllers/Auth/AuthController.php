<?php

namespace App\Controllers\Auth;

use App\Controllers\Controller;
use App\Models\User;
use Respect\Validation\Validator as v;
use App\Auth\Auth;

class AuthController extends Controller
{
    public function getSignUp($request, $response) {
        // Renderiza o formulário de registro
        return $this->view->render($response, 'auth/signup.html');
    }
    
    public function postSignUp($request, $response) {
        
        // Informa as regras de validação do formulário
        $validation = $this->validator->validate($request, [
            'email' => v::email()->notEmpty()->emailAvailable(),
            'name' => v::notEmpty()->charset('UTF-8')->not(v::numeric()),
            'password' => v::noWhitespace()->notEmpty(),
        ]);
        
        // Verifica se houve erro de validação
        if ($validation->failed()) {
            // Ocorrendo erro, redireciona para o formulário de registrp
            return $response->withRedirect($this->router->pathFor('auth.signup'));
        }
        
        // Não ocorrendo erro, inclui os dados informados no Banco de Dados
        $user = User::create([
            'email' => $request->getParam('email'),
            'name' => $request->getParam('name'),
            'password' => password_hash($request->getParam('password'), PASSWORD_DEFAULT),
        ]);
        
        // Cria mensagem avisando o novo usuário foi criado
        $this->flash->addMessage('success', 'Novo usuário criado com sucesso!');
        
        // Realiza a Autenticação do Usuário e inicia a sessão
        // No caso da senha, passa o parâmetro como Plain Text, conforme digitado pelo usuário
        // Por segurança, JAMAIS usa a senha após o hash do sistema
        $this->auth->attempt($user->email, $request->getParam('password'));
        
        // Após salvar os dados no Banco, redireciona para a home da administração
        return $response->withRedirect($this->router->pathFor('admin.home'));
    }
    
    
    public function getSignIn($request, $response) {
        // Renderiza o formulário de login
        return $this->view->render($response, 'auth/signin.html');
    }
    
    public function postSignIn($request, $response) {
        // Envia dados do formulário para autenticação        
        $auth = $this->auth->attempt(
            $request->getParam('email'),
            $request->getParam('password')
        );
        // Verifica se o usuário está autenticado
        if (!$auth) {
            // Gera uma mensagem de erro
            $this->flash->addMessage('danger', 'Não foi possível completar a autenticação do usuário. Verifique seu e-mail e sua senha.');
            
            // Se não estiver autenticado, retorna para a página de login
            return $response->withRedirect($this->router->pathFor('auth.signin'));
        }
        
        // Se estiver autenticado, acessa a home da administração
        return $response->withRedirect($this->router->pathFor('admin.home'));
        
    }
    
    public function getSignOut($request, $response) {
        $this->auth->logout();
        return $response->withRedirect($this->router->pathFor('index'));
    }    

}
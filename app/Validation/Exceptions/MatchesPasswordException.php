<?php

namespace App\Validation\Exceptions;

use Respect\Validation\Exceptions\ValidationException;

class MatchesPasswordException extends ValidationException
{
    public static $defaultTemplates = [
        self::MODE_DEFAULT => [
            self::STANDARD => 'Senha Incorreta! Informe a senha atual do usuário corretamente.',
        ],
        self::MODE_NEGATIVE => [
            self::STANDARD => 'Senha Correta! Informe outra senha.',
        ],
    ];
}
<?php

namespace App\Validation\Exceptions;

use Respect\Validation\Exceptions\ValidationException;

class EmailAvailableException extends ValidationException
{
    public static $defaultTemplates = [
        self::MODE_DEFAULT => [
            self::STANDARD => 'O e-mail informado já está registrado. Efetue login ou informe outro e-mail.',
        ],
        self::MODE_NEGATIVE => [
            self::STANDARD => 'O e-mail informado não está registrado. Informe outro e-mail.',
        ],
    ];
}
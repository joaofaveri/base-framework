<?php

namespace App\Validation;

use Respect\Validation\Validator as Respect;
use Respect\Validation\Exceptions\NestedValidationException;

class Validator
{
    // Caso ocorra erro de validação, serão armazenados na variável abaixo
    protected $errors;
    
    // Faz a validação dos campos de formulário, de acordo com as regras
    public function validate($request, array $rules) {
        
        foreach ($rules as $field => $rule) {
            try {
                // Aplica as regras de validação ao campo correspondente do formulário
                $rule->setName(ucfirst($field))->assert($request->getParam($field));
            } catch (NestedValidationException $exception) {
                $this->errors[$field] = $exception->getMessages();
            }
        }
        
        // Persistir os error encontrados através de uma Sessão
        $_SESSION['errors'] = $this->errors;
        
        return $this;
    }
    
    // 
    public function failed() {
        return !empty($this->errors);
    }
}
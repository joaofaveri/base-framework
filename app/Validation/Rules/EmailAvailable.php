<?php

namespace App\Validation\Rules;

use Respect\Validation\Rules\AbstractRule;
use App\Models\User;

class EmailAvailable extends AbstractRule
{
    public function validate($input) {
        
        // Deve retorna true ou false
        // True, quando passar na validação, ou seja, não existir o e-mail registrado
        // False, quando não passar na validação, ou seja, o e-mail já foi registrado
        return User::where('email', $input)->count() === 0;
        
    }
}
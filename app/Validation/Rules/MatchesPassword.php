<?php

namespace App\Validation\Rules;

use Respect\Validation\Rules\AbstractRule;
use App\Models\User;

class MatchesPassword extends AbstractRule
{
    protected $password;
    
    public function __construct($password) {
        $this->password = $password;
    }
    
    public function validate($input) {
        
        // Deve retorna true ou false
        // True, quando passar na validação, ou seja, a senha informado coincide com o BD
        // False, quando não passar na validação, ou seja, a senha não coincide com o BD
        return password_verify($input, $this->password);
    }
}
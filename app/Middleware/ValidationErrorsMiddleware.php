<?php

namespace App\Middleware;

class ValidationErrorsMiddleware extends Middleware
{
    public function __invoke($request, $response, $next) {
        
        // Código antes do Slim manipular a requisição
        // $response->getBody()->write('BEFORE');
        
        if (isset($_SESSION['errors'])) {
            // Recupera os erros encontrados a partir da Sessão
            $this->container->view->getEnvironment()->addGlobal('errors', $_SESSION['errors']);
            // Esvazia a variável na Sessão, pois ela não é mais necessária
            unset($_SESSION['errors']);
        }
        
        $response = $next($request, $response);
        // Código depois do Slim manipular a requisição
        // $response->getBody()->write('AFTER');
        
        return $response;
    }
}

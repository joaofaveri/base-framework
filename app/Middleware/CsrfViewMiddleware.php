<?php

namespace App\Middleware;

class CsrfViewMiddleware extends Middleware
{
    public function __invoke($request, $response, $next) {
        // CSRF token name and value
        $nameKey = $this->container->csrf->getTokenNameKey();
        $valueKey = $this->container->csrf->getTokenValueKey();
        $name = $request->getAttribute($nameKey);
        $value = $request->getAttribute($valueKey);
        
        // Cria uma variável global
        $this->container->view->getEnvironment()->addGlobal('csrf', [
            'field' => '
                <input type="hidden" name="' . $nameKey . '" value="'. $name .'">
                <input type="hidden" name="' . $valueKey . '" value="'. $value .'">
                ',
            ]);
        
        $response = $next($request, $response);
        return $response;
    }
}
<?php

namespace App\Middleware;

class AuthMiddleware extends Middleware
{
    public function __invoke($request, $response, $next) {
        
        // Verifica se o usuário está logado na Aplicação
        if (!$this->container->auth->check()) {
            
            // Se não estiver logado, gera uma mensagem de erro
            $this->container->flash->addMessage('danger', 'Por favor, efetue o login antes de prosseguir.');
            
            // Se não estiver logado, redireciona para a página de login
            return $response->withRedirect($this->container->router->pathFor('auth.signin'));
        }
        
        $response = $next($request, $response);
        return $response;
    }
}
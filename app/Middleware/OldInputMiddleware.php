<?php

namespace App\Middleware;

class OldInputMiddleware extends Middleware
{
    public function __invoke($request, $response, $next) {
        
        // Cria uma variável global para receber os dados antigos postados no Formulário
        $this->container->view->getEnvironment()->addGlobal('old', $_SESSION['old']);
        // Seta a sessão com os Parâmetros recebidos via post
        $_SESSION['old'] = $request->getParams();
        
        $response = $next($request, $response);
        return $response;
    }
}
<?php

namespace App\Middleware;

class GuestMiddleware extends Middleware
{
    public function __invoke($request, $response, $next) {
        
        // Verifica se o usuário está logado na Aplicação
        if ($this->container->auth->check()) {
            
            // Se estiver logado, redireciona para a home da administração
            // O objetivo é evitar que um usuário já autenticado visite
            // as páginas de login ou de registro
            return $response->withRedirect($this->container->router->pathFor('admin.home'));
        }
        
        $response = $next($request, $response);
        return $response;
    }
}
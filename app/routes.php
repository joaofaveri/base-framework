<?php

use App\Middleware\AuthMiddleware;
use App\Middleware\GuestMiddleware;

// Routes
$app->get('/', function ($request, $response) {
    // $this->logger->info("Acesso à rota '/'");
    $response->getBody()->write("Página Inicial para Testes!");
    return $response;
})->setName('index');

// Testando Twig-View
$app->get('/hello/{name}', function ($request, $response, array $args) {
    return $this->view->render($response, 'hello/hello.html', [
        'name' => $args['name'],
    ]);
});

// Grupo para rotas que serão evitadas para usuários já autenticados
$app->group('', function() {
    // Rota para o Formulário de Registro de Usuário
    $this->get('/registro', 'AuthController:getSignUp')->setName('auth.signup');
    $this->post('/registro', 'AuthController:postSignUp');
    // Rota para o Formulário de Login de Usuário
    $this->get('/login', 'AuthController:getSignIn')->setName('auth.signin');
    $this->post('/login', 'AuthController:postSignIn');
})->add(new GuestMiddleware($container));

// Grupo para rotas que exigem autenticação do usuário
$app->group('', function() {
    // Rota com Controllers e DIC
    $this->get('/home', 'HomeController:index')->setName('admin.home');
    // Rotas para o Formulário de Alteração de Senha
    $this->get('/password', 'PasswordController:getChangePassword')->setName('password.change');
    $this->post('/password', 'PasswordController:postChangePassword');
})->add(new AuthMiddleware($container));

// Rota para o Logout de Usuário
$app->get('/logout', 'AuthController:getSignOut')->setName('auth.signout');
<?php

namespace App\Auth;

use App\Models\User;

class Auth
{
    // Retorna uma instância do usuário autenticado
    public function user() {
        if ($this->check()) {
            return User::find($_SESSION['user']);
        }
    }
    
    // Verifica se o Usuário está autenticado
    public function check() {
        return isset($_SESSION['user']);
    }
    
    // Função de Autenticação de Usuário
    // Return false or true
    public function attempt($email, $password) {
        
        // Verificar se o usuário já está registrado na Aplicação
        $user = User::where('email', $email)->first();
        
        // Se o usuário não existe, retorna false
        if (!$user) {
            return false;
        }
        
        // Se o usuário existe, verifica se a senha está correta
        // Se usuário e senha estiverem corretos, inicia a sessão e retona true
        if (password_verify($password, $user->password)) {
            $_SESSION['user'] = $user->id;
            return true;
        }
        
        // Retorna false se a senha estiver incorreta
        return false;
        
    }
    
    // Função para o Logout do usuário da Aplicação
    public function logout() {
        unset($_SESSION['user']);
    }
}
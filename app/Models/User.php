<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    // Tabela a ser operada pela classe
    protected $table = 'users';
    
    // Mass Assignment Exception
    protected $fillable = [
        'name',
        'email',
        'password',
    ];
    
    // Realiza a atualização da senha do usuário
    public function setPassword($password) {
        $this->update([
            'password' => password_hash($password, PASSWORD_DEFAULT)
        ]);
    }
}